## Usecase
This script is intended to help with developing sites on your local linux machine.

It will help you with managing your localhosts and assumes that:
1. You setup one database per website
2. You host the files on /var/www/SITE

It has only been tested under Ubuntu 16.04, and assumes that you have apache2 and mysql already installed.

Examples:
```bash
sudo ./localhosts.py add newproject
```
Will result in:
1. A new directory /var/www/newproject created
2. A new mysql database newproject created
3. A new entry to your hosts file so you can access the site on http://newproject
4. A new apache virtualhost file created /etc/apache2/sites-available/newproject.conf
5. The new apache site enalbed and apache restarted.


```bash
sudo ./localhosts.py remove newproject
```
Will revert the previous steps, prompting for confirmation on deleting the DB and directory.


```bash
sudo ./localhosts.py list
```
Will list the virtualhosts currently on the system