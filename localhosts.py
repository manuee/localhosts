#!/usr/bin/python

"""
Manages apache virtual hosts on linux
"""

import sys
import os
import argparse
import commands
from shutil import rmtree
import logging
import MySQLdb as db

logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)


def execute_command(command_string):
    """
    Helper function to execute shell commands.
    """
    (status, output) = commands.getstatusoutput(command_string)
    if status:
        sys.stderr.write(output + '\n')
        return False
    return True


def clean_site_arg(sitename):
    """
    Cleans up a sitename never include .conf extension
    """
    return os.path.splitext(sitename)[0]


def query_yes_no(question, default="yes"):
    """
    http://code.activestate.com/recipes/577058/
    Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is one of "yes" or "no".
    """
    valid = {"yes": "yes", "y": "yes", "ye": "yes",
             "no": "no", "n": "no"}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while 1:
        sys.stdout.write(question + prompt)
        choice = raw_input().lower()
        if default is not None and choice == '':
            return default
        elif choice in valid.keys():
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")


class Localhosts:
    def __init__(self, hosts_dir='/etc/hosts', apache_conf_dir='/etc/apache2',
                 webserver_root='/var/www'):
        self.hosts_dir = hosts_dir
        self.apache_conf_dir = apache_conf_dir
        self.webserver_root = webserver_root
        self.db_user = 'admin'
        self.db_pass = 'admin'
        self.db_host = 'localhost'
        conn = self.get_database_connection()
        if not conn:
            logging.error("Could not connect to MySql.")
            return False
        self.dbconn = conn

    def remove_site(self, sitename):
        self.remove_virtualhost(sitename)
        self.remove_hosts_site(sitename)
        self.remove_webroot_dir(sitename)
        self.drop_database(sitename)

    def add_site(self, sitename):
        self.add_webroot_dir(sitename)
        self.add_virtualhost(sitename)
        self.add_hosts_site(sitename)
        self.create_database(sitename)

    def get_database_connection(self):
        try:
            conn = db.connect(host=self.db_host, user=self.db_user, passwd=self.db_pass)
        except db.Error as e:
            logging.error("Error %d: %s" % (e.args[0], e.args[1]))
            return False
        return conn

    def database_exists(self, sitename):
        cursor = self.dbconn.cursor()
        sql = 'SHOW DATABASES LIKE \'' + sitename + '\''
        cursor.execute(sql)
        dbresult = cursor.fetchone()
        cursor.close()
        return dbresult and dbresult[0]

    def create_database(self, sitename):
        if self.database_exists(sitename):
            logging.warning('Database already exists: %s', sitename)
            self.dbconn.close()
        else:
            try:
                cursor = self.dbconn.cursor()
                sql = 'CREATE DATABASE ' + sitename + ';'
                cursor.execute(sql)
                cursor.close()
                self.dbconn.close()
                logging.info("Created database: %s", sitename)
            except db.Error as e:
                logging.error("Error %d: %s" % (e.args[0], e.args[1]))
                return False

    def drop_database(self, sitename):
        print("")
        if query_yes_no('Would you like to drop the database ' +
                        sitename + '?', 'no') == 'yes':
            if self.database_exists(sitename):
                try:
                    cursor = self.dbconn.cursor()
                    sql = 'DROP DATABASE ' + sitename + ';'
                    cursor.execute(sql)
                    cursor.close()
                    self.dbconn.close()
                    logging.info("Dropped database: %s", sitename)
                except db.Error as e:
                    logging.error("Error %d: %s" % (e.args[0], e.args[1]))
                    return False
            else:
                logging.warning('Database does not exist: %s', sitename)
                self.dbconn.close()

    def remove_virtualhost(self, sitename):
        if self.site_exists(sitename):
            if execute_command('sudo a2dissite ' + sitename):
                logging.info('Disabled apache site: %s', sitename)
            if execute_command('sudo service apache2 reload'):
                logging.info('Reloaded apache')
            else:
                logging.error('Apache failed to reload!')

            os.remove('/etc/apache2/sites-available/' + sitename + '.conf')
            logging.info('Removed apache virtualhost file: %s.conf', sitename)
        else:
            logging.warning('Apache virtualhost file not found: %s.conf',
                            sitename)

    def remove_hosts_site(self, sitename):
        with open(self.hosts_dir, 'r') as f:
            data = f.readlines()
            f.close()
        line_num = 0
        hosts_removed = False
        for line in data:
            if sitename in line:
                data[line_num] = line.replace("127.0.0.1 " + sitename, "")
                hosts_removed = True
            line_num += 1
        if hosts_removed:
            with open(self.hosts_dir, 'w') as f:
                f.writelines(data)
                f.close()
            logging.info('Site removed from hosts file: %s', sitename)
        else:
            logging.warning('Site not found on hosts file: %s', sitename)

    def remove_webroot_dir(self, sitename):
        hostdir = self.webserver_root + '/' + sitename
        print("")
        if query_yes_no('Would you like to remove the directory ' +
                        hostdir + ' and its contents?', 'no') == 'yes':
            if os.path.exists(hostdir):
                rmtree(hostdir)
                logging.info('Removed the directory: %s', hostdir)
            else:
                logging.info('Directory not found: %s', hostdir)
        else:
            logging.info('Directory not removed: %s', hostdir)

    def add_webroot_dir(self, sitename):
        # Create the webhost directory if necessary.
        hostdir = self.webserver_root + '/' + sitename
        if not os.path.exists(hostdir):
            os.makedirs(hostdir)
            logging.info('Created directory: %s', hostdir)
        else:
            logging.warning('Directory already exists: %s', hostdir)

    def add_virtualhost(self, sitename):
        if not self.site_exists(sitename):
            # Create the new apache virtualhost file.
            conf_file = self.apache_conf_dir + '/sites-available/' + sitename + '.conf'
            docroot = self.webserver_root + '/' + sitename
            f = open(conf_file, "w")
            f.write('<VirtualHost *:80>\n')
            f.write('    ServerName ' + sitename + '\n')
            f.write('    DocumentRoot ' + docroot + '\n')
            f.write('    <Directory ' + docroot + '>\n')
            f.write('        Options Indexes FollowSymLinks MultiViews\n')
            f.write('        AllowOverride All\n')
            f.write('        Order allow,deny\n')
            f.write('        allow from all\n')
            f.write('    </Directory>\n')
            f.write('    ErrorLog ${APACHE_LOG_DIR}/' + sitename +
                    '_error.log\n')
            f.write('    LogLevel warn\n')
            f.write('</VirtualHost>\n')
            f.close()
            logging.info('Created apache virtualhost file: %s', conf_file)
        else:
            logging.warning('Virtualhost file already exists for site: %s', sitename)
        # Enable the new apache site and reload.
        if execute_command('sudo a2ensite ' + sitename):
            logging.info('Enabled apache site: %s', sitename)
        else:
            logging.error('Failed to enable the site %s.conf', sitename)

        if execute_command('sudo service apache2 reload'):
            logging.info('Reloaded apache')
        else:
            logging.error('Apache failed to reload!')

    def add_hosts_site(self, sitename):
        # Add entry to hosts file
        already_in_hosts = False
        f = open(self.hosts_dir, "rw+")
        for line in f.readlines():
            if sitename in line:
                logging.warning('Site already in hosts file: %s', sitename)
                already_in_hosts = True
        if not already_in_hosts:
            f.write("127.0.0.1 " + sitename + '\n')
            logging.info('Site added to hosts file: http://%s', sitename)
        f.close()

    def site_exists(self, sitename):
        """
        Verifies whether an apache virtual host configuration file exists or not
        """
        return sitename + '.conf' in self.list_virtual_hosts()

    def list_virtual_hosts(self, enabled=False):
        """
        Lists apache virtual hosts on the system
        """
        path = self.apache_conf_dir + '/sites-available'
        if enabled:
            path = self.apache_conf_dir + '/sites-enabled'

        return os.listdir(path)


def main():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest='subcommand')
    parser_add = subparsers.add_parser('add', help='add a new virtual host configuration')
    parser.add_argument('--apacheconf', default='/etc/apache2', nargs='?',
                        help='the path to your apache configuration directory (default: %(default)s)')

    parser_add.add_argument('sitenames', help='names of the sites to add', nargs='+')
    parser_add.add_argument('-hf', '--hostsfile', default='/etc/hosts', nargs='?',
                            help='the path to your hosts file (default: %(default)s)')
    parser_add.add_argument('-wr', '--webserveroot', default='/var/www', nargs='?',
                            help='the path to where you want the new site located (default: %(default)s)')

    parser_remove = subparsers.add_parser('remove', help='remove an existing virtual host configuration')
    parser_remove.add_argument('sitenames', help='names of the sites to remove', nargs='+')
    parser_remove.add_argument('-hf', '--hostsfile', default='/etc/hosts', nargs='?',
                               help='the path to your hosts file  (default: %(default)s)')
    parser_remove.add_argument('-wr', '--webserveroot', default='/var/www', nargs='?',
                               help='the path to where the existing site is located (default: %(default)s)')

    parser_list = subparsers.add_parser('list', help='show apache virtual hosts')
    parser_list.add_argument('-e', '--enabled', help='list only enabled apache virtualhosts', action='store_true',
                             default=False)

    args = parser.parse_args()

    if args.subcommand == 'list':
        manager = Localhosts(apache_conf_dir=args.apacheconf)
        print('\n'.join(manager.list_virtual_hosts(enabled=args.enabled)))
    else:
        if os.getuid() != 0:
            logging.error("You must run this program as sudo to add or remove sites.")
            sys.exit(1)
        manager = Localhosts(hosts_dir=args.hostsfile,
                             apache_conf_dir=args.apacheconf,
                             webserver_root=args.webserveroot)
    if args.subcommand == 'add':
        for sitename in args.sitenames:
            clean_sitename = clean_site_arg(sitename)
            manager.add_site(clean_sitename)

    elif args.subcommand == 'remove':
        for sitename in args.sitenames:
            clean_sitename = clean_site_arg(sitename)
            manager.remove_site(clean_sitename)


if __name__ == '__main__':
    main()
